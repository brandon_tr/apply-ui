import React from 'react'
import ReactDOM from 'react-dom'
import Amplify from 'aws-amplify'
import awsExports from './aws-exports'
import './index.css'
import {AuthenticatedApp} from './App'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(
    <React.StrictMode>
        <AuthenticatedApp />
    </React.StrictMode>,
    document.getElementById('root')
)

Amplify.configure(awsExports)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
