import React from 'react'
import {UnauthenticatedApp} from './App'
import redirect from './services/redirect'
import {mount} from 'enzyme'

it('should redirect when component mounts', () => {
  window.location.href = 'https://www.talentreef.com?redirect=http://newurl.com'
  redirect.navigate = jest.fn()

  mount(<UnauthenticatedApp />)
  expect(redirect.navigate).toBeCalledWith('http://newurl.com')
})
