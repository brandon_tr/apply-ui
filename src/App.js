import React from 'react'
import logo from './tf-logo-white.png'
import './App.css'
import {withAuthenticator} from '@aws-amplify/ui-react'
import redirect from './services/redirect'

class App extends React.Component {
  componentDidMount() {
    redirect.navigate(redirect.parseUrl())
  }

  render() {
    return <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
      </header>
    </div>
  }
}

const AuthenticatedApp = withAuthenticator(App)
const UnauthenticatedApp = App
export {
  AuthenticatedApp,
  UnauthenticatedApp,
}
