import url from 'url'

export default {
  parseUrl: () => {
    const redirectUrl = url
      .parse(window.location.href, true)
      .query.redirect

    try {
      return new URL(redirectUrl).href
    } catch (err) {
      console.error('redirect uri is malformed', err)
      return undefined
    }
  },

  navigate: (url) => {
    if (url)
      window.location.assign(url)
  }
}
