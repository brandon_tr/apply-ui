import redirect from './redirect'

describe('parseUrl', () => {
  it('should parse redirect uri', () => {
    expect(redirect.parseUrl('http://testurl.com?redirect=http://newurl.com/path'))
      .toBe('http://newurl.com/path')
  })

  it('should parse redirect uri with multiple split characters', () => {
    expect(redirect.parseUrl('http://test.com/?redirect=http://newurl.com?somethingelse=1234'))
      .toBe('http://newurl.com/?somethingelse=1234')
  })

  it('should return undefined when redirect url is not present', () => {
    expect(redirect.parseUrl('http://test.com/'))
      .toBe(undefined)
  })

  it('should return undefined when redirect url is not valid', () => {
    expect(redirect.parseUrl('http://test.com?redirect=notaurl'))
      .toBe(undefined)
  })

  it('should return undefined when redirect url is not present', () => {
    expect(redirect.parseUrl('http://test.com?shmedirect=notaurl'))
      .toBe(undefined)
  })
})

describe('navigate', () => {
  it('should redirect user', () => {
    window.location.assign = jest.fn()

    const redirectUrl = 'https://www.newurl.com/'
    redirect.navigate(redirectUrl)

    expect(window.location.assign).toHaveBeenCalledWith(redirectUrl)
  })

  it('should not redirect if redirect url is not provided', () => {
    window.location.assign = jest.fn()

    redirect.navigate('')
    expect(window.location.assign).not.toHaveBeenCalled()

    redirect.navigate(null)
    expect(window.location.assign).not.toHaveBeenCalled()

    redirect.navigate(undefined)
    expect(window.location.assign).not.toHaveBeenCalled()
  })
})
