import axios from 'axios'

export default {
    get: async (url, token, headers) => {
        if (token)
            return axios.get(url, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    ...headers
                },
            })
                .then(response => response)
        return axios.get(url, h)
            .then(response => response)
    },
    post: async (url, token, headers) => {
        if (token)
            return axios.post(url, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    ...headers
                }
            })
        return axios.post(url)
    }
}
