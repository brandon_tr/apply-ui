import http from './default'
import axios from 'axios'

describe('default http client', () => {
    describe('get', () => {
        it('should include headers', () => {
            const token = 'token'
            const url = 'http://test.url'
            const headers = {additional: 'header'}
            http.get(url, token, headers)
            expect(axios.get).toHaveBeenCalledWith(url, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    additional: 'header'
                }
            })
        })

        it('should make get requests with token', () => {
            const token = 'token'
            const url = 'http://test.url'
            http.get(url, token)
            expect(axios.get).toHaveBeenCalledWith(url, {headers: {'Authorization': `Bearer ${token}`}})
        })

        it('should makes get requests', () => {
            const url = 'http://test.url'
            http.get(url)
            expect(axios.get).toHaveBeenCalledWith(url)
        })
    })

    describe('post', () => {
        it('should include headers', () => {
            const headers = {additional: 'header'}
            const token = 'token'
            const url = 'http://test.url'
            http.post(url, token, headers)
            expect(axios.post).toHaveBeenCalledWith(url, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    additional: 'header'
                }
            })
        })

        it('should make post requests with token', () => {
            const token = 'token'
            const url = 'http://test.url'
            http.post(url, token)
            expect(axios.post).toHaveBeenCalledWith(url, {headers: {'Authorization': `Bearer ${token}`}})
        })

        it('should makes post requests', () => {
            const url = 'http://test.url'
            http.post(url)
            expect(axios.post).toHaveBeenCalledWith(url)
        })
    })
})
