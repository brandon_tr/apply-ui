context('account sign in', () => {
    before(() => cy.visit(''))

    it('should sign in', () => {
        cy.get('[data-test="sign-in-username-input"]', {includeShadowDom: true})
            .type(`test.talentreef@gmail.com`)

        cy.get('[data-test="sign-in-password-input"]', {includeShadowDom: true})
            .type('Password1!')

        cy.get('[data-test="sign-in-sign-in-button"]', {includeShadowDom: true})
            .first()
            .click()
    })
})
