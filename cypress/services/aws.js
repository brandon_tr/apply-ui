import mailslurp from './mailslurp'

const userPoolId = Cypress.env('user_pool_id')
export default {
    deleteUser: () => cy.exec(`aws cognito-idp admin-delete-user --user-pool-id ${userPoolId} --username ${mailslurp.email}`)
}
